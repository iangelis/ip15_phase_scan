from tree_maker import initialize
import time
import os
import itertools
import numpy as np
import yaml
import shutil
import copy
import json
from user_defined_functions import (
    generate_run_sh,
    generate_run_sh_htc,
    get_worst_bunch,
    reformat_filling_scheme_from_lpc_alt,
)

# Define dictionary for the initial particle distribution
d_config_particles = {}

# Radius of the initial particle distribution
d_config_particles["r_min"] = 2
d_config_particles["r_max"] = 10
d_config_particles["n_r"] = (
    2 * 16 * (d_config_particles["r_max"] - d_config_particles["r_min"])
)

# Number of angles for the initial particle distribution
d_config_particles["n_angles"] = 5

# Number of split for parallelization
d_config_particles["n_split"] = 1


### Mad configuration

# Define dictionary for the Mad configuration
d_config_mad = {
    "beam_config": {"lhcb1": {}, "lhcb2": {}},
    "links": {},
    "config_ip15_phase": {},
}


# Optic file path (version, and round or flat)

### For v1.6 optics
d_config_mad["links"]["acc-models-lhc"] = "../../../../../modules/hllhc16"
d_config_mad["links"]["summer_optics"] = "../../../../../modules/summer_optics"
# d_config_mad["optics_file"] = "acc-models-lhc/strengths/ramp/opt_ramp_500_1500_thin.madx"
d_config_mad["optics_file"] = "summer_optics/collapse/opt_collapse_1100_1500_thin.madx"
d_config_mad["ver_hllhc_optics"] = 1.6

# Beam energy (for both beams)
beam_energy_tot = 7000
d_config_mad["beam_config"]["lhcb1"]["beam_energy_tot"] = beam_energy_tot
d_config_mad["beam_config"]["lhcb2"]["beam_energy_tot"] = beam_energy_tot

# Define dictionnary for ip15 phases
d_config_ip15_phase = {
    "mux": {},
    "muy": {},
}

# NOTE: Should the defaults be here?
d_config_ip15_phase["mux"]["lhcb1"] = -30.9955
d_config_ip15_phase["mux"]["lhcb2"] = -31.0411
d_config_ip15_phase["muy"]["lhcb1"] = -29.9689
d_config_ip15_phase["muy"]["lhcb2"] = -29.9896

# Add ip15 phases
d_config_mad["config_ip15_phase"] = d_config_ip15_phase

### Tune and chroma configuration

# Define dictionnary for tune and chroma
d_config_tune_and_chroma = {
    "qx": {},
    "qy": {},
    "dqx": {},
    "dqy": {},
}
for beam in ["lhcb1", "lhcb2"]:
    d_config_tune_and_chroma["qx"][beam] = 62.31
    d_config_tune_and_chroma["qy"][beam] = 60.32
    d_config_tune_and_chroma["dqx"][beam] = 15.0
    d_config_tune_and_chroma["dqy"][beam] = 15.0

# Value to be added to linear coupling knobs
d_config_tune_and_chroma["delta_cmr"] = 0.001
d_config_tune_and_chroma["delta_cmi"] = 0.0

### Knobs configuration

# Define dictionary for the knobs settings
d_config_knobs = {}

# Knobs at IPs
d_config_knobs["on_x1"] = 250
d_config_knobs["on_sep1"] = 0
d_config_knobs["on_x2"] = -170
d_config_knobs["on_sep2"] = 0.138
d_config_knobs["on_x5"] = 250
d_config_knobs["on_sep5"] = 0
d_config_knobs["on_x8h"] = 0.0
d_config_knobs["on_x8v"] = 170

# Crab cavities
d_config_knobs["on_crab1"] = 0
d_config_knobs["on_crab5"] = 0

# Octupoles
d_config_knobs["i_oct_b1"] = 60.0
d_config_knobs["i_oct_b2"] = 60.0

### leveling configuration

# Leveling in IP 1/5
d_config_leveling_ip1_5 = {"constraints": {}}
d_config_leveling_ip1_5["luminosity"] = 2.0e34
d_config_leveling_ip1_5["constraints"]["max_intensity"] = 2.3e11
d_config_leveling_ip1_5["constraints"]["max_PU"] = 160


# Define dictionary for the leveling settings
d_config_leveling = {
    "ip2": {},
    "ip8": {},
}

# Luminosity and particles


# Leveling parameters (ignored if skip_leveling is True)
d_config_leveling["ip2"]["separation_in_sigmas"] = 5
d_config_leveling["ip8"]["luminosity"] = 2.0e33

### Beam beam configuration

# Define dictionary for the beam beam settings
d_config_beambeam = {"mask_with_filling_pattern": {}}

# Beam settings
d_config_beambeam["num_particles_per_bunch"] = 1.4e11
d_config_beambeam["nemitt_x"] = 2.5e-6
d_config_beambeam["nemitt_y"] = 2.5e-6

# Filling scheme (in json format)
# The scheme should consist of a json file containing two lists of booleans (one for each beam),
# representing each bucket of the LHC.
filling_scheme_path = os.path.abspath(
    "templates/filling_scheme/8b4e_1972b_1960_1178_1886_224bpi_12inj_800ns_bs200ns.json"
)

# Alternatively, one can get a fill directly from LPC from, e.g.:
# https://lpc.web.cern.ch/cgi-bin/fillTable.py?year=2023
# In this page, get the fill number of your fill of interest, and use it to replace the XXXX in the
# URL below before downloading:
# https://lpc.web.cern.ch/cgi-bin/schemeInfo.py?fill=XXXX&fmt=json
# Unfortunately, the format is not the same as the one used by defaults in xmask, but it should
# still be converted in the lines below (see with matteo.rufolo@cern.ch for questions, or if it
# doesn't work).

# Load filling scheme
if filling_scheme_path.endswith(".json"):
    with open(filling_scheme_path, "r") as fid:
        d_filling_scheme = json.load(fid)

# If the filling scheme is already in the correct format, do nothing
if "beam1" in d_filling_scheme.keys() and "beam2" in d_filling_scheme.keys():
    pass
# Otherwise, we need to reformat the file
else:
    # One can potentially use b1_array, b2_array to scan the bunches later
    b1_array, b2_array = reformat_filling_scheme_from_lpc_alt(filling_scheme_path)
    filling_scheme_path = filling_scheme_path.replace(".json", "_converted.json")


# Add to config file
d_config_beambeam["mask_with_filling_pattern"][
    "pattern_fname"
] = filling_scheme_path  # If None, a full fill is assumed

# Set this variable to False if you intend to scan the bunch number (but ensure both bunches indices
# are defined later)
d_config_beambeam["mask_with_filling_pattern"]["i_bunch_b1"] = None
d_config_beambeam["mask_with_filling_pattern"]["i_bunch_b2"] = None
check_bunch_number = True
if check_bunch_number:
    # Bunch number (ignored if pattern_fname is None (in which case the simulation considers all bunch
    # elements), must be specified otherwise)
    # If the bunch number is None and pattern_name is defined, the bunch with the largest number of
    # long-range interactions will be used
    if d_config_beambeam["mask_with_filling_pattern"]["i_bunch_b1"] is None:
        # Case the bunch number has not been provided
        worst_bunch_b1 = get_worst_bunch(
            filling_scheme_path, numberOfLRToConsider=26, beam="beam_1"
        )
        while d_config_beambeam["mask_with_filling_pattern"]["i_bunch_b1"] is None:
            bool_inp = input(
                "The bunch number for beam 1 has not been provided. Do you want to use the bunch"
                " with the largest number of long-range interactions? It is the bunch number "
                + str(worst_bunch_b1)
                + " (y/n): "
            )
            if bool_inp == "y":
                d_config_beambeam["mask_with_filling_pattern"][
                    "i_bunch_b1"
                ] = worst_bunch_b1
            elif bool_inp == "n":
                d_config_beambeam["mask_with_filling_pattern"]["i_bunch_b1"] = int(
                    input("Please enter the bunch number for beam 1: ")
                )

    if d_config_beambeam["mask_with_filling_pattern"]["i_bunch_b2"] is None:
        worst_bunch_b2 = get_worst_bunch(
            filling_scheme_path, numberOfLRToConsider=26, beam="beam_2"
        )
        # For beam 2, just select the worst bunch by default, as the tracking of b2 is not available yet anyway
        print(
            "The bunch number for beam 2 has not been provided. By default, the worst bunch is"
            " taken. It is the bunch number " + str(worst_bunch_b2)
        )

        d_config_beambeam["mask_with_filling_pattern"]["i_bunch_b2"] = worst_bunch_b2


# ==================================================================================================
# --- Generate dictionnary to encapsulate all base collider parameters (generation 2)
# ==================================================================================================
d_config_collider = {}

# Add tunes and chromas
d_config_collider["config_knobs_and_tuning"] = d_config_tune_and_chroma

# Add phase
d_config_collider["config_ip15_phase"] = d_config_ip15_phase

# Add knobs
d_config_collider["config_knobs_and_tuning"]["knob_settings"] = d_config_knobs

# Add luminosity configuration
d_config_collider["config_lumi_leveling_ip1_5"] = d_config_leveling_ip1_5
d_config_collider["config_lumi_leveling"] = d_config_leveling

# Add beam beam configuration
d_config_collider["config_beambeam"] = d_config_beambeam
# Machine parameters to be rematched

array_dqx = np.round(np.linspace(31.2, 31.5, 5), decimals=4)[:2]
array_dqy = np.round(np.linspace(30.15, 30.5, 5), decimals=4)[:1]

# array_dqx = np.array([31.2])
# array_dqy = np.array([30.238])

# Add phase steps to generate optics
d_config_mad["config_ip15_phase"]["muxs"] = array_dqx.tolist()
d_config_mad["config_ip15_phase"]["muys"] = array_dqy.tolist()


# Build empty tree: first generation (later added to the root), and second generation
children = {"base_collider": {"config_particles": {}, "config_mad": {}, "children": {}}}

# Add particles distribution parameters to the first generation
children["base_collider"]["config_particles"] = d_config_particles

# Add base machine parameters to the first generation
children["base_collider"]["config_mad"] = d_config_mad


d_config_simulation = {}

# Number of turns to track
d_config_simulation["n_turns"] = 200

# Initial off-momentum
d_config_simulation["delta_max"] = 27.0e-5

# Beam to track (lhcb1 or lhcb2)
d_config_simulation["beam"] = "lhcb1"


dump_collider = True
dump_config_in_collider = True


track_array = np.arange(d_config_particles["n_split"])
for idx_job, (track, dmux, dmuy) in enumerate(
    itertools.product(track_array, array_dqx, array_dqy)
):
    for beam in ["lhcb1", "lhcb2"]:
        d_config_collider["config_ip15_phase"]["mux"][beam] = float(dmux)
        d_config_collider["config_ip15_phase"]["muy"][beam] = float(dmuy)

    d_config_simulation["particle_file"] = f"../particles/{track:02}.parquet"

    d_config_simulation[
        "collider_file"
    ] = f"./collider_phase_{dmux:2.3f}_{dmuy:2.3f}.json"

    children["base_collider"]["children"][f"xtrack_{idx_job:04}"] = {
        "config_mad": copy.deepcopy(d_config_mad),
        "config_simulation": copy.deepcopy(d_config_simulation),
        "config_collider": copy.deepcopy(d_config_collider),
        "log_file": "tree_maker.log",
        "dump_collider": dump_collider,
        "dump_config_in_collider": dump_config_in_collider,
    }

# Load the tree_maker simulation configuration
config = yaml.safe_load(open("config.yaml"))

# # Set the root children to the ones defined above
config["root"]["children"] = children

# Optics study name
# optics_study_name = "test_phase_ip15"
optics_study_name = "run_test_ip15"

# Set miniconda environment path in the config
config["root"]["setup_env_script"] = os.getcwd() + "/../activate_miniforge.sh"

# Creade folder that will contain the tree
if not os.path.exists("rematching/" + optics_study_name):
    os.makedirs("rematching/" + optics_study_name)

# Move to the folder that will contain the tree
os.chdir("rematching/" + optics_study_name)

# Clean the id_job file
id_job_file_path = "id_job.yaml"
if os.path.isfile(id_job_file_path):
    os.remove(id_job_file_path)

root = initialize(config)

# Check if htcondor is the configuration
if "htc" in config["root"]["generations"][2]["run_on"]:
    generate_run = generate_run_sh_htc
else:
    generate_run = generate_run_sh


root.make_folders(generate_run)
