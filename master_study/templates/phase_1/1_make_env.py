import xmask as xm
import yaml


def load_configuration(config_path="config.yaml"):
    # Load configuration
    with open(config_path, "r") as fid:
        configuration = yaml.safe_load(fid)

    config_mad = configuration["config_mad"]
    config_collider = configuration["config_collider"]

    return configuration, config_mad, config_collider


if __name__ == "__main__":
    configuration, config_mad, config_collider = load_configuration()
    # Make mad environment
    xm.make_mad_environment(links=config_mad["links"])
