import build_collider
import test_track


if __name__ == "__main__":
    build_collider.build_phase_colliders()
    build_collider.build_distr_and_colliders()
    test_track.configure_and_track()
