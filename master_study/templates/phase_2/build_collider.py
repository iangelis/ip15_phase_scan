"""This script is used to build the base collider with Xmask, configuring only the optics. Functions
in this script are called sequentially."""
# ==================================================================================================
# --- Imports
# ==================================================================================================
from cpymad.madx import Madx
import os
import xmask as xm
import xmask.lhc as xlhc
import shutil
import json
import yaml
import logging
import numpy as np
import itertools
import pandas as pd
import tree_maker
import xtrack as xt
import xtrack._temp.lhc_match as lm
import xpart as xp

# Import user-defined optics-specific tools
import optics_specific_tools as ost


# ==================================================================================================
# --- Function for tree_maker tagging
# ==================================================================================================
def tree_maker_tagging(config, tag="started"):
    # Start tree_maker logging if log_file is present in config
    if tree_maker is not None and "log_file" in config:
        tree_maker.tag_json.tag_it(config["log_file"], tag)
    else:
        logging.warning("tree_maker loging not available")


# ==================================================================================================
# --- Function to load configuration file
# ==================================================================================================
def load_configuration(config_path="config.yaml"):
    # Load configuration
    with open(config_path, "r") as fid:
        configuration = yaml.safe_load(fid)

    config_mad = configuration["config_mad"]
    config_collider = configuration["config_collider"]

    return configuration, config_mad, config_collider


# ==================================================================================================
# --- Function to build collider from mad model
# ==================================================================================================
def build_collider_from_mad(config_mad, sanity_checks=True):
    # Start mad
    mad_b1b2 = Madx(command_log="mad_collider.log")

    mad_b4 = Madx(command_log="mad_b4.log")

    # Build sequences
    ost.build_sequence(mad_b1b2, mylhcbeam=1)
    ost.build_sequence(mad_b4, mylhcbeam=4)

    # Apply optics (only for b1b2, b4 will be generated from b1b2)
    ost.apply_optics(mad_b1b2, optics_file=config_mad["optics_file"])

    if sanity_checks:
        mad_b1b2.use(sequence="lhcb1")
        mad_b1b2.twiss()
        ost.check_madx_lattices(mad_b1b2)
        mad_b1b2.use(sequence="lhcb2")
        mad_b1b2.twiss()
        ost.check_madx_lattices(mad_b1b2)

    # Apply optics (only for b4, just for check)
    ost.apply_optics(mad_b4, optics_file=config_mad["optics_file"])
    if sanity_checks:
        mad_b4.use(sequence="lhcb2")
        mad_b4.twiss()
        ost.check_madx_lattices(mad_b1b2)

    # Build xsuite collider
    collider = xlhc.build_xsuite_collider(
        sequence_b1=mad_b1b2.sequence.lhcb1,
        sequence_b2=mad_b1b2.sequence.lhcb2,
        sequence_b4=mad_b4.sequence.lhcb2,
        beam_config=config_mad["beam_config"],
        enable_imperfections=config_mad["enable_imperfections"],
        enable_knob_synthesis=config_mad["enable_knob_synthesis"],
        rename_coupling_knobs=config_mad["rename_coupling_knobs"],
        pars_for_imperfections=config_mad["pars_for_imperfections"],
        ver_lhc_run=config_mad["ver_lhc_run"],
        ver_hllhc_optics=config_mad["ver_hllhc_optics"],
    )
    collider.build_trackers()

    if sanity_checks:
        collider["lhcb1"].twiss(method="4d")
        collider["lhcb2"].twiss(method="4d")
    # Return collider
    return collider


def activate_RF_and_twiss(collider, config_mad, sanity_checks=True):
    # Define a RF system (values are not so immportant as they're defined later)
    print("--- Now Computing Twiss assuming:")
    if config_mad["ver_hllhc_optics"] == 1.6:
        dic_rf = {"vrf400": 16.0, "lagrf400.b1": 0.5, "lagrf400.b2": 0.5}
        for knob, val in dic_rf.items():
            print(f"    {knob} = {val}")
    elif config_mad["ver_lhc_run"] == 3.0:
        dic_rf = {"vrf400": 12.0, "lagrf400.b1": 0.5, "lagrf400.b2": 0.0}
        for knob, val in dic_rf.items():
            print(f"    {knob} = {val}")
    print("---")

    # Rebuild tracker if needed
    try:
        collider.build_trackers()
    except:
        print("Skipping rebuilding tracker")

    for knob, val in dic_rf.items():
        collider.vars[knob] = val

    if sanity_checks:
        for my_line in ["lhcb1", "lhcb2"]:
            ost.check_xsuite_lattices(collider[my_line])

    return collider


def clean():
    # Remove all the temporaty files created in the process of building collider
    os.remove("mad_collider.log")
    os.remove("mad_b4.log")
    shutil.rmtree("temp")
    os.unlink("errors")
    os.unlink("acc-models-lhc")


# ==================================================================================================
# --- Function to start generation of colliders
# ==================================================================================================
def match_ip15_phase(collider, tar_mux15, tar_muy15, staged_match=True, solve=True):
    tw0 = collider.twiss()
    mux_15_orig_b1 = tw0["lhcb1"][:, "ip1"].mux[0] - tw0["lhcb1"][:, "ip5"].mux[0]
    muy_15_orig_b1 = tw0["lhcb1"][:, "ip1"].muy[0] - tw0["lhcb1"][:, "ip5"].muy[0]
    mux_15_orig_b2 = tw0["lhcb2"][:, "ip1"].mux[0] - tw0["lhcb2"][:, "ip5"].mux[0]
    muy_15_orig_b2 = tw0["lhcb2"][:, "ip1"].muy[0] - tw0["lhcb2"][:, "ip5"].muy[0]

    refqxb1 = tw0["lhcb1"].qx
    refqyb1 = tw0["lhcb1"].qy
    refqxb2 = tw0["lhcb2"].qx
    refqyb2 = tw0["lhcb2"].qy

    if mux_15_orig_b1 < 0:
        mux_15_orig_b1 += refqxb1
    if muy_15_orig_b1 < 0:
        muy_15_orig_b1 += refqyb1
    if mux_15_orig_b2 < 0:
        mux_15_orig_b2 += refqxb2
    if muy_15_orig_b2 < 0:
        muy_15_orig_b2 += refqyb2

    print(f"{mux_15_orig_b1=}")
    print(f"{muy_15_orig_b1=}")
    print(f"{mux_15_orig_b2=}")
    print(f"{muy_15_orig_b2=}")

    d_mux_15_b1 = mux_15_orig_b1 - tar_mux15
    d_muy_15_b1 = muy_15_orig_b1 - tar_muy15
    d_mux_15_b2 = mux_15_orig_b2 - tar_mux15
    d_muy_15_b2 = muy_15_orig_b2 - tar_muy15

    print(f"{tar_mux15=}, {tar_muy15=}")
    print(f"{d_mux_15_b1=}, {d_muy_15_b1=}")
    print(f"{d_mux_15_b2=}, {d_muy_15_b2=}")

    opts = ost.change_ip15_phase(
        collider,
        dqx=tar_mux15,
        dqy=tar_muy15,
        d_mux_15_b1=d_mux_15_b1,
        d_muy_15_b1=d_muy_15_b1,
        d_mux_15_b2=d_mux_15_b2,
        d_muy_15_b2=d_muy_15_b2,
        staged_match=staged_match,
        solve=solve,
    )

    return collider, opts


def build_phase_colliders(config_file="config.yaml"):
    configuration, config_mad, config_collider = load_configuration(config_file)

    tar_mux15 = config_collider["config_ip15_phase"]["mux"]["lhcb1"]
    tar_muy15 = config_collider["config_ip15_phase"]["muy"]["lhcb1"]

    # Make mad environment
    xm.make_mad_environment(links=config_mad["links"])

    # Start mad
    mad_b1b2 = Madx(command_log="mad_collider.log")

    mad_b4 = Madx(command_log="mad_b4.log")

    # Build sequences
    ost.build_sequence(mad_b1b2, mylhcbeam=1, ignore_cycling=True)
    ost.build_sequence(mad_b4, mylhcbeam=4, ignore_cycling=True)

    # Apply optics (only for b1b2, b4 will be generated from b1b2)
    ost.apply_optics(mad_b1b2, optics_file=config_mad["optics_file"])
    mad_b1b2.use("lhcb1")
    mad_b1b2.twiss()

    # Apply optics (only for b4, just for check)
    ost.apply_optics(mad_b4, optics_file=config_mad["optics_file"])

    mad_b4.use("lhcb2")
    mad_b4.twiss()

    line1 = xt.Line.from_madx_sequence(
        mad_b1b2.sequence.lhcb1,
        allow_thick=True,
        deferred_expressions=True,
        replace_in_expr={"bv_aux": "bvaux_b1"},
    )

    line4 = xt.Line.from_madx_sequence(
        mad_b4.sequence.lhcb2,
        allow_thick=True,
        deferred_expressions=True,
        replace_in_expr={"bv_aux": "bvaux_b2"},
    )

    # Remove solenoids (cannot backtwiss for now)
    for ll in [line1, line4]:
        tt = ll.get_table()
        for nn in tt.rows[tt.element_type == "Solenoid"].name:
            ee_elen = ll[nn].length
            ll.element_dict[nn] = xt.Drift(length=ee_elen)

    collider = xt.Multiline(lines={"lhcb1": line1, "lhcb2": line4})
    collider.lhcb1.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)
    collider.lhcb2.particle_ref = xp.Particles(mass0=xp.PROTON_MASS_EV, p0c=7000e9)

    collider.lhcb1.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["method"] = "4d"
    collider.lhcb2.twiss_default["reverse"] = True

    collider.build_trackers()

    match_ip15_phase(collider, tar_mux15, tar_muy15, staged_match=True, solve=True)
    lm.gen_madx_optics_file_auto(
        collider, f"opt_phase_ip15_{tar_mux15:2.3f}_{tar_muy15:2.3f}.madx"
    )


def build_distr_and_colliders(config_file="config.yaml"):
    configuration, config_mad, config_collider = load_configuration(config_file)

    tar_mux15 = config_collider["config_ip15_phase"]["mux"]["lhcb1"]
    tar_muy15 = config_collider["config_ip15_phase"]["muy"]["lhcb1"]

    # Get sanity checks flag
    # sanity_checks = configuration["sanity_checks"]
    # For now, optics files from xtrack missing some variables (e.g. qxBIM)
    sanity_checks = False

    # Tag start of the job
    tree_maker_tagging(configuration, tag="started")

    optics_file = f"opt_phase_ip15_{tar_mux15:2.3f}_{tar_muy15:2.3f}.madx"
    config_mad["optics_file"] = optics_file

    # Build collider from mad model
    collider = build_collider_from_mad(config_mad, sanity_checks)

    # Twiss to ensure eveyrthing is ok
    collider = activate_RF_and_twiss(collider, config_mad, sanity_checks)

    # Clean temporary files
    clean()

    # Save collider to json
    collider.to_json(f"collider_phase_{tar_mux15:2.3f}_{tar_muy15:2.3f}.json")

    # Tag end of the job
    tree_maker_tagging(configuration, tag="completed")


# ==================================================================================================
# --- Script for execution
# ==================================================================================================

if __name__ == "__main__":
    build_phase_colliders()
    build_distr_and_colliders()
